# Imagen raíz
FROM node

# Carpeta raíz
WORKDIR /apitechu

# Copia de archivos
ADD . /apitechu

# Exponer puerto
EXPOSE 3000

# Instalar dependencias (comentado a proposito... para producción debiera ser sin comentar)
# RUN npm install

# Añadir volumen
VOLUME ['/logs']

# Comando de inicialización
CMD ["npm", "start"]
