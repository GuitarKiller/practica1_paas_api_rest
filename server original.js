var express=require('express');
var app=express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var baseMlabURL="https://api.mlab.com/api/1/databases/apitechugk/collections/";
var mLabAPIKey="apiKey=HOATVyVnI9arWgN8D1LNWX2PNzxFxtUQ";
var requestJson= require('request-json');

app.listen(port);
console.log("API escuchando en el puerto ... " + port);

/*Añadir una ruta con una función   */
app.get('/apitechu/v1',
  function(req, res) {
    console.log("GET /apitechu/v1");
    res.send(
      {
      "msg": "TechU Rest Yeaahhhh¡¡¡"
      }
    );
  }
);

/*Añadir una ruta con una función   */
app.get('/apitechu/v1/users',
  function(req, res) {
    console.log("GET /apitechu/v1/users");
    res.sendFile('usuarios.json',{root: __dirname});
    //res.sendfile('./usuarios.json'); deprecated
  }
);

/**/
app.get('/apitechu/v2/users',
  function(req, res) {
    console.log("GET /apitechu/v2/users");
    httpClient=requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");

    httpClient.get("user?" + mLabAPIKey,
    function(err,resMLab,body){
        var response= !err ? body : {
          "msg":"Error obteniendo usuarios."
        }
        res.send(response);
    }
  );
  }
);

app.get('/apitechu/v2/users/:id',
  function(req, res) {
    // Obtención de usuario a partir de un id
    console.log("GET /apitechu/v2/users/:id");

    var id  =req.params.id;
    var query = 'q={"id":'+id+'}';

    httpClient=requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");

    httpClient.get("user?" + query + "&"+ mLabAPIKey,
    function(err,resMLab,body){
        if(err) {
           response={
            "msg":"Error obteniendo usuario " + id
          };
          res.status(500);
        }
        else {
          if (body.length>0){
            response=body[0];
          }
          else {
            response = {
          "msg" : "Usuario " + id + " no encontrado."
          };
          res.status(404);
          }
        }

        res.send(response);
    }
  );
  }
);


/*Añadir una ruta con una función   */
app.post('/apitechu/v1/users',
  function(req, res) {
    console.log("POST /apitechu/v1/users");
  /*  var newUser = {
      "first_name": req.headers.first_name,
      "last_name" : req.headers.last_name,
      "country" :  req.headers.country
    }*/
    var newUser = {
       "first_name": req.body.first_name,
       "last_name" : req.body.last_name,
       "country" :  req.body.country
     }
    //Trazas
    //console.log("first_name es " + req.headers.first_name);
    //console.log("last_name es " + req.headers.last_name);
    //console.log("country es " + req.headers.country);

    var users = require ('./usuarios.json');
    users.push (newUser);
    writeUserDataToFile(users);
    //console.log("Usuario " + req.headers.first_name + " añadido con éxito");
    console.log("Usuario " + req.body.first_name + " añadido con éxito");
    /*res.send(
    {
      "msg": "Usuario " + req.headers.first_name + " añadido con éxito"
    })*/
    //res.send(users);
  }
);

/*Añadir una ruta con una función   */
app.delete('/apitechu/v1/users/:id',
  function(req, res) {
    console.log("DELETE /apitechu/v1/users/:id");
    var fecha = new Date();
    var users = require ('./usuarios.json');
    users.splice(req.params.id-1,1);
    /*
    users.splice(req.params.id-1,1); // Eliminamos 1 registro desde el id solicitado
    */

    if (users.splice.length<req.params.id)  // Controlamos si el id solicitado es superior al nº de elementos existentes
      {
        res.send(
          {
            "msg": fecha + " - Usuario " + req.params.id + " no existe"
          }
        );
      }
    else {
        writeUserDataToFile(users);
        res.send(
          {
            "msg": "Usuario " + req.params.id + " eliminado con éxito" + limite
          });
      console.log("Usuario " + req.params.id + " eliminado con éxito");
    }
  }
);


/*Añadir una ruta con una función   */
app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res) {
    console.log("POST /apitechu/v1/monstruo");
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);


function writeUserDataToFile(data){
  var fs=require('fs');
  var jsonUserData=JSON.stringify(data);

  fs.writeFile("./usuarios.json",jsonUserData,"utf8",
  function(err){
    if (err){
        console.log(err);
      }
    else {
      console.log("Fichero de usuarios persistido");
    }d
  });
}

function writeLogonProperty(id){
  httpClient=requestJson.createClient(baseMlabURL);
  console.log("Cliente creado");

  var query = 'q={"id":'+id+'}';
  var putBody = '{"$set":{"logged":true}}';

  httpClient.put("user?" + query + "&"+ mLabAPIKey,JSON.parse(putBody),
  function(err,resMLab,body){
          response = {
        "msg" : "Usuario logado."
      };
      });

  };


app.post('/apitechu/v1/login',
  function(req, res) {
    console.log("POST /apitechu/v1/login");
    //Trazas
    console.log(req.body.email);
    console.log(req.body.password);

    var logged=false; // Booleano para controlar logon
    var users = require ('./usuarios.json');
    for (user of users) {
      if ((user.email==req.body.email) && (user.password==req.body.password)) {
        logged=true;
        break;
      }
    }
    if (logged==true) {
      /*Mas trazas*/
      console.log(user.id);
      console.log("Usuario encontrado");
      console.log(user.logged);

      user.logged=true; //Hemos encontrado al usuario y las password es ok
      writeUserDataToFile(users);
      // Devolvemos respuesta
      res.send(
      {
        "msg": "Login correcto",
        "idUsuario" : user.id
      })
    }
    else {
      console.log("Usuario no encontrado o password incorrecta");
      res.send(
      {
        "msg": "Login incorrecto"
      })
    }
    res.send(users);
  }
);

app.post('/apitechu/v2/login',
  function(req, res) {
    console.log("POST /apitechu/v2/login");
    //Trazas
    console.log(req.body.email);
    console.log(req.body.password);

    var query = 'q={"email":"'+req.body.email+'","password":"'+req.body.password+'"}';

    httpClient=requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");

    httpClient.get("user?" + query + "&"+ mLabAPIKey,
    function(err,resMLab,body){

        if(err) {
           response={
            "msg":"Error obteniendo usuario " + req.body.email
          };
          estado=500;
        }
        else {
          if (body.length>0){
            response=body[0];
            writeLogonProperty(body[0].id);
            estado=200;
          }
          else {
            response = {
          "msg" : "Usuario " + req.body.email + " no encontrado."
          };
          estado=404;
          }
        }
        res.status(estado);
        res.send(response);

    }
  );
}
);

app.post('/apitechu/v1/logout',
  function(req, res) {
    console.log("POST /apitechu/v1/logout");
    //Trazas
    console.log(req.body.id); //Trazas

    var logout=false;//Booleano para controlar logout
    var users = require ('./usuarios.json');
    for (user of users) {
      if ((user.id==req.body.id) && (user.logged==true)) {
        logout=true;
        break;
      }
    }
    if (logout==true) {
      //Mas Trazas
      console.log(user.id);
      console.log("Usuario logado");
      console.log(user.logged);
      delete user.logged;// Eliminamos la propiedad
      writeUserDataToFile(users);
      res.send(
      {
        "msg": "Logout correcto",
        "idUsuario" : user.id
      })
    }
    else {
      console.log("Usuario no encontrado o no logado");
      res.send(
      {
        "msg": "Logout incorrecto"
      })
    }
    res.send(users);
  }
);

app.post('/apitechu/v2/logout/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/logout/:id");

   var query = 'q={"id": ' + req.params.id + '}';
   console.log("query es " + query);

   httpClient=requestJson.createClient(baseMlabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Got a user with that id, logging out");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)

app.get('/apitechu/v2/users/:id/account',
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id/account");

    var id  =req.params.id;
    var query = 'q={"id":'+id+'}';

    httpClient=requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");

    httpClient.get("account?" + query + "&"+ mLabAPIKey,
    function(err,resMLab,body){
        if(err) {
           response={
            "msg":"Error obteniendo cuentas del usuario " + id
          };
          res.status(500);
        }
        else {
          if (body.length>0){
            response=body;
          }
          else {
            response = {
          "msg" : "Cuentas de Usuario " + id + " no encontrado."
          };
          res.status(404);
          }
        }
        res.send(response);
        // res.status(estado);
    }
  );
  }
);
