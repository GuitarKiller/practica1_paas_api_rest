var express=require('express');
var app=express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;


var baseMlabURL="https://api.mlab.com/api/1/databases/apitechugk/collections/";
var mLabAPIKey="apiKey=HOATVyVnI9arWgN8D1LNWX2PNzxFxtUQ";
var requestJson= require('request-json');


app.listen(port);
console.log("API escuchando en el puerto ... " + port);

/*Función para comprobar funcionamiento del API*/
app.get('/apitechu/v1',
  function(req, res) {
    console.log("GET /apitechu/v1");
    res.send(
      {
      "msg": "TechU Rest Yeaahhhh¡¡¡",
      }
    );
  }
);

/*Creación de usuario*/
app.post('/apitechu/v1/users',
  function(req, res) {
    console.log("POST /apitechu/v1/users");
    var newUser = {
       "id":-1,
       "first_name": req.body.first_name,
       "last_name" : req.body.last_name,
       "email" :  req.body.email.toLowerCase(),
       "password" : req.body.password
    }
    // Cliente
    httpClient=requestJson.createClient(baseMlabURL);
    //Chequeamos si el cliente ya existe (Busqueda Email)
    var query = 'q={"email":"'+req.body.email.toLowerCase()+'"}';
    httpClient.get("user?" + query + "&"+ mLabAPIKey,
    function(err,resMLab1,body){
        if(err) {//Existe un problema en la llamada al API Rest de MLab
            console.log(req.body.email + " Existe un problema en la llamada al API Rest de MLab.");
            response={
              "msg":"Error comprobando usuario " + req.body.email
            };
            estado=500;
            res.status(estado);
            res.send(response);
          }
        else {
          if (body.length>0){//Usuario ya existe. No es posible realizar el alta.
            console.log(req.body.email + " Usuario ya existe. No es posible realizar el alta.");
            response={
              "msg":"Operación alta usuario no autorizada. Usuario ya existe " + req.body.email
            };
            estado=401;
            res.status(estado);
            res.send(response);
          }
          else {// El usuario no existe. Continuamos con el proceso de alta
            console.log(req.body.email + "El usuario no existe. Continuamos con el proceso de alta")
            httpClient.get("user?c=true&" + mLabAPIKey,
            function(err,resMLab2,body){
              response= !err ? body : {
                  "msg":"Error obteniendo usuarios."
                }
                var ultimoCliente=Number(body)+1;
                // Ya tenemos el id del nuevo cliente, procedemos a invocar el alta
                newUser.id=ultimoCliente;
                console.log(ultimoCliente + " Nuevo Id de cliente obtenido.")
                // Obtenemos nº de nueva cuenta para el cliente
                var ibanNuevaCuenta=getNewAccount();
                // Comprobamos que la cuenta está libre
                var query = 'q={"iban":"'+ibanNuevaCuenta+'"}';
                httpClient.get("account?" + query + "&"+ mLabAPIKey,
                function(err,resMLab1,body){
                    if(err) {//Existe un problema en la llamada al API Rest de MLab
                        console.log(req.body.email + " Existe un problema en la llamada al API Rest de MLab.");
                        response={
                          "msg":"Error comprobando usuario " + req.body.email
                        };
                        estado=500;
                        res.status(estado);
                        res.send(response);
                      }
                    else {
                      if (body.length>0){//La cuenta ya existe. No es posible realizar el alta.
                        console.log(req.body.email + " Cuenta ya existe. No es posible realizar el alta. Reintente la operación. ");
                        response={
                          "msg":"Operación alta usuario no autorizada. Cuenta ya existe " + req.body.email
                        };
                        estado=401;
                        res.status(estado);
                        res.send(response);
                      }
                      else {// La cuenta esta disponible. Continuamos con el proceso de alta
                        console.log(req.body.email + "Cuenta ok.Continuamos con el proceso de alta");
                        // Persistimos usuario
                        writeUserDataToDb(newUser);
                        console.log("Usuario creado.");
                        // Vinculamos Cuenta
                        var newAccount = {
                              "id" : ultimoCliente,
                              "iban" : ibanNuevaCuenta,
                              "balance" : 0,
                              "numeroMovimientos" : 0
                            };
                        writeAccountDataToDb(newAccount);

                        }
                      }
                  //Preparamos respuesta
                  response={
                    "msg":"Operación alta realizada. Id Cliente " + ultimoCliente + " - IBAN - " + ibanNuevaCuenta
                  };
                  estado=200;
                  res.status(estado);
                  res.send(response);});
              }
            );
           }
        }
    }
  );
  }
);

/*Creación de movimiento*/
app.post('/apitechu/v1/accountingMovement',
  function(req, res) {
    console.log("POST /apitechu/v1/accountingMovement");
    var newAccountingMovement = {
       "iban": req.body.iban,
       "tipo": req.body.tipo,
       "cantidad" : req.body.cantidad,
       "ibanDestino": req.body.ibanDestino
    };
    // Cliente
    console.log(newAccountingMovement);
    httpClient=requestJson.createClient(baseMlabURL);
    //Chequeamos si la cuenta existe (Busqueda iban)
    var query = 'q={"iban":"'+req.body.iban+'","id":'+req.body.id+'}';
    console.log(query);
    httpClient.get("account?" + query + "&"+ mLabAPIKey,
      function(err,resMLab1,body){
        if(err) {//Existe un problema en la llamada al API Rest de MLab
            console.log(req.body.iban + " Existe un problema en la llamada al API Rest de MLab.");
            response={
              "msg":"Error comprobando cuenta " + req.body.iban
            };
            estado=500;
            res.status(estado);
            res.send(response);
        }
        else {
          if (body.length>0){//Cuenta origen existe. Continuamos.
            console.log(req.body.iban + " Cuenta origen existe. Continuamos.");
            response={
              "msg":"Movimiento contabilizado " + req.body.tipo + " - cantidad " + req.body.cantidad
            };
            if (chequeoSaldo (body[0].balance,newAccountingMovement.cantidad,newAccountingMovement.tipo)) {
                console.log("Saldo suficiente");
                  if ((newAccountingMovement.tipo=='recibo')||(newAccountingMovement.tipo=='transferencia')){
                    newAccountingMovement.cantidad=newAccountingMovement.cantidad*-1;
                    console.log(newAccountingMovement.cantidad);
                  }
                  if (newAccountingMovement.tipo=='transferencia'){
                    /*Comprobamos si existe la cuenta destino*/
                    if (newAccountingMovement.ibanDestino==null) {
                      newAccountingMovement.ibanDestino='xxx';
                    }
                    var query = 'q={"iban":"'+req.body.ibanDestino+'"}';
                    console.log(query);
                    httpClient.get("account?" + query + "&"+ mLabAPIKey,
                    function(err,resMLab2,body){
                        if(err) {//Existe un problema en la llamada al API Rest de MLab
                            console.log(newAccountingMovement.ibanDestino + " Existe un problema en la llamada al API Rest de MLab.");
                            response={
                              "msg":"Error comprobando cuenta " + newAccountingMovement.ibanDestino
                            };
                            estado=500;
                            res.status(estado);
                            res.send(response);
                          }
                        else {
                          if (body.length>0){//Cuenta destino existe. Continuamos.
                            console.log("Destino encontrada");
                            // Realizamos el movimiento en la cuenta inicial.
                            writeNewAccountingMovement(newAccountingMovement);
                            // Si es una transferencia, ingresamos la cantidad en la cuenta destino
                            var newAccountingMovementDestino = {
                               "iban": newAccountingMovement.ibanDestino,//"ES06 0182 9444 7143 2553 1188"
                               "tipo": newAccountingMovement.tipo,
                               "cantidad" : (newAccountingMovement.cantidad*-1)
                            };
                            writeNewAccountingMovement(newAccountingMovementDestino);
                            response={
                              "msg":"Movimiento contabilizado " + newAccountingMovement.tipo + " - cantidad " + newAccountingMovement.cantidad + " de cuenta "+
                              newAccountingMovement.iban + " a cuenta destino " + newAccountingMovement.ibanDestino
                            };
                            estado=200;
                            res.status(estado);
                            res.send(response);
                          }
                          else {
                            response={
                              "msg":"Cuenta destino no encontrada " + newAccountingMovement.ibanDestino
                          };
                          estado=404;
                          res.status(estado);
                          res.send(response);
                          }
                        }
                      })
                    }
                    else {
                      writeNewAccountingMovement(newAccountingMovement);
                      estado=200;
                      res.status(estado);
                      res.send(response);
                    }
                  }
                  else {
                    response={
                      "msg":"Operación no permitida. Saldo insuficiente."
                  };
                  estado=401;
                  res.status(estado);
                  res.send(response);
                }
              }
              else {
                response={
                  "msg":"Cuenta origen no encontrada " + req.body.iban
              };
              estado=404;
              res.status(estado);
              res.send(response);
              }
            }
          }
          )
        });

// login
app.post('/apitechu/v2/login',
  function(req, res) {
    console.log("POST /apitechu/v2/login");
    //Trazas
    console.log("**" + req.body.email);
    console.log(req.body.password);

    var query = 'q={"email":"'+req.body.email+'","password":"'+req.body.password+'"}';

    httpClient=requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");

    httpClient.get("user?" + query + "&"+ mLabAPIKey,
    function(err,resMLab,body){
        if(err) {
            response={
            "msg":"Error obteniendo usuario " + req.body.email
            };
            estado=500;
            console.log("Salgo por error");
          }
        else {
          if (body.length>0){
              response=body[0];
              var fechaActual=new Date().getTime();
              var sesionActiva=fechaActual-body[0].fechaUltimoLogin;
              var secreto='';
              if ((body[0].logged)&&(sesionActiva<300000)){
                //La sesion es aún
                secreto=body[0].secreto;
                }
              else
                { //La sesión no es valida o no está iniciada asignamos una nueva
                  var secreto=obtieneSecreto(body[0].id);
                  writeLogonProperty(body[0].id,secreto);
                }
            response=
              {
                "login" : true,
                "id" : body[0].id,
                "first_name": body[0].first_name,
                "last_name": body[0].last_name,
                "secreto": secreto,
                "fechaUltimoLogin": new Date().getTime(),
                "msg" : "Usuario " + body[0].email + " logado."
              };
            estado=200;
          }
          else {
            response = {
          "msg" : "Usuario " + req.body.email + " no encontrado."
          };
          estado=404;
          }
        }
        console.log(estado);
        res.status(estado);
        res.send(response);

    }
  );
}
);
// Logout
app.post('/apitechu/v2/logout/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/logout/:id");

   var query = 'q={"id": ' + req.params.id + '}';
   console.log("query es " + query);

   httpClient=requestJson.createClient(baseMlabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Got a user with that id, logging out");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":"","secreto":"","fechaUltimoLogin":""}}'
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "mensaje" : "Usuario deslogado con éxito",
               "id" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)
//Obtiene cuentas a partir de un Id dado
app.get('/apitechu/v2/users/:id/account',
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id/account");

    var id  =req.params.id;
    var query = 'q={"id":'+id+'}';

    httpClient=requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");

    httpClient.get("account?" + query + "&"+ mLabAPIKey,
    function(err,resMLab,body){
        if(err) {
           response={
            "msg":"Error obteniendo cuentas del usuario " + id
          };
          res.status(500);
        }
        else {
          if (body.length>0){
            response=body;
          }
          else {
            response = {
          "msg" : "Cuentas de Usuario " + id + " no encontrado."
          };
          res.status(404);login
          }
        }
        res.send(response);
        // res.status(estado);
    }
  );
  }
);
  /*Funciones locales*/
  /*Numero de cuenta aleatorio*/
  function getNewAccount() {
    Number.prototype.pad = function(size) {
      var s = String(this);
      while (s.length < (size || 2)) {s = "0" + s;}
      return s;
    }
    var iban;
    const banco='0182';
    var digitoControlIBAN= Math.floor(Math.random()* (99-1)+1);
    var oficina= Math.floor(Math.random()* (9999-1)+1);
    var digitoControl= Math.floor(Math.random()* (99-1)+1);
    var numeroCuenta=  String(new Date().getTime()).substring(3, 13);
    iban= "ES"+digitoControlIBAN.pad(2)+ " " + banco + " " + oficina.pad(4) + " " + digitoControl.pad(2)
      + numeroCuenta.substring(0,2) + " " + numeroCuenta.substring(2,6) + " " + numeroCuenta.substring(6,10);
    return iban;
  }
  /*Grabar en bd*/
  function writeUserDataToDb(data){
    httpClient=requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");
    var jsonUserData=JSON.stringify(data);
    // console.log(jsonUserData);
    var insercion=httpClient.post("user?" + mLabAPIKey,JSON.parse(jsonUserData),
    function(err,resMLab,body){
      response = {
        "msg" : "Usuario creado."
      };
    });
    }
  /*Persistimos cuenta en colección cuenta*/
  function writeAccountDataToDb(data){
    httpClient=requestJson.createClient(baseMlabURL);
    var jsonAccountData=JSON.stringify(data);
    console.log(jsonAccountData);
    httpClient.post("account?" + mLabAPIKey,JSON.parse(jsonAccountData),
    function(err,resMLab,body){
      response = {
        "msg" : "Cuenta vinculada."
      };
      });
    }
  /*Generamos secreto para grabar al logarse el usuario*/
  function obtieneSecreto(id){
    httpClient=requestJson.createClient(baseMlabURL);
    console.log("WP-Cliente creado");
    var secreto = new Date().getTime();
    console.log(secreto);
    var data = id.toString + secreto;
    var crypto = require('crypto');
    var clave=crypto.createHash('md5').update(data).digest("hex");
    return clave;
  }
  /*Grabar logged:true en coleccion usuario a partir de un id dado*/
  function writeLogonProperty(id,clave){
    var query = 'q={"id":'+id+'}';
    var fechaUltimoLogin=new Date().getTime();
    var putBody = '{"$set":{"logged":true,"secreto":"'+clave+'","fechaUltimoLogin":"'+fechaUltimoLogin+'"}}';
    httpClient.put("user?" + query + "&"+ mLabAPIKey,JSON.parse(putBody),
    function(err,resMLab,body){
      response = {
        "msg" : "Usuario logado."
      };
    });
  }
  /*Función chequeoSaldo*/
  function chequeoSaldo (balance,cantidad,tipoMovimiento)
  {
    if ((tipoMovimiento=='recibo')||(tipoMovimiento=='transferencia')){
      cantidad=cantidad*-1;
    }
    var saldo=balance+cantidad;
    if (saldo<0)
    {return false;}
    else {
      return true;
    }
  }

  /*Grabar movimiento en coleccion acoount*/
  function writeNewAccountingMovement(data){
    httpClient=requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");
    //Chequeamos si la cuenta tiene movimientos (Busqueda  x iban)
    var query = 'q={"iban":"'+data.iban+'"}';
    httpClient.get("account?" + query + "&"+ mLabAPIKey,
    function(err,resMLab1,body){
        if(err) {//Existe un problema en la llamada al API Rest de MLab
            console.log(data.iban + " Existe un problema en la llamada al API Rest de MLab.");
            response={
              "msg":"Error comprobando cuenta " + data.iban
            };
            estado=500;
            resMLab1.status(estado);
            resMLab1.send(response);
          }
        else {
          if (body.length>0){//Cuenta existe. Continuamos.
            // Inserción movimiento
            var fecha= new Date().toDateString();
            console.log(fecha);
            var query = 'q={"iban":"'+data.iban+'"}';
            var putBody = '{"$push":{"movimientos":  {"movimiento" : [{"idMov": "'+ Number(body[0].numeroMovimientos+1) +'",'+
            '"Fecha": "'+fecha+'", "tipo": "' + data.tipo +'", "cantidad": '+data.cantidad+'}]}},"$inc":{"numeroMovimientos":1,"balance":'+(Math.round(data.cantidad*100)/100)+'}}';
            console.log(data.iban + " Cuenta existe. Continuamos.");
            console.log(putBody);
            console.log("Movimiento número:"+ Number(body[0].numeroMovimientos+1));
            httpClient.put("account?" + query + "&"+ mLabAPIKey,JSON.parse(putBody),
            function(err,resMLab,body){
                    response = {
                  "msg" : "Movimiento persistido."
                };
                console.log("Post - Movimiento persistido");
              }
            );
          }
        }
      }
    )
    };
